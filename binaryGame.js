(function(){
  
  $("button.play").click(function(){
    $(this).animate( {height: "0px", opacity: "0"} );
    $("div.pane").each( function(){
      $(this).animate( {height: "150px" } );
    }, game() );
  });

  $("div.input input").change(function(){
    var question = $(".question").find("span").text();
    if( $(".question").hasClass("binary") ){
      var answerd = bd( question );
      $(this).val() == bd( question ) ? correct() : wrong(answerd);
    }
    else{
      var answerb = db( question );
      $(this).val() == db( question ) ? correct() : wrong(answerb);
    }
  });

  function correct(){
    alert("Correct!");
    setTimeout(
      function(){ game(); },
      1000
    );
  }

  function wrong(answer){
    alert("The correct answer was " + answer + " .");
    setTimeout(
      function(){ game(); },
      1000
    );
  }

  function alert(msg){
    $("div.alert").text(msg).animate( {height: "50px", opacity: "1"} );
    setTimeout(
      function(){
        $("div.alert").animate( {height: "0px", opacity: "0"} ); 
      },
      1000
    );      
  }

  function game(){
    var type = Math.round( Math.random() );
    var num = "";
    switch(type){
      case 0:
        for(i=0; i<= Math.floor( Math.random() * 23 + 1 ); i++){
          num += Math.round( Math.random() ).toString();
        } 
        $("div.question").addClass("binary").removeClass("decimal");
        $("div.question h3").text("Convert from binary:");
        break;
      case 1:
        num = Math.floor( Math.random() * 23 + 1 ).toString(); $("div.question").addClass("decimal").removeClass("binary");
        $("div.question h3").text("Convert from decimal:");
        break;
    };
    $("div.question span").text( num );
    $("div.input input").val("");
  }

  function bd( text ){ 
    var output = 0;
    text = text.split("").reverse().join().replace(/,/g , "");
    for(var digit in text){ 
      if( text[digit] == "1" ){ 
        output += Math.pow(2, digit); 
      }
    }
    return output.toString();
  }

  function db( num ){
    var output = "";
    num = Math.floor( num );
    while( num >= 2 ){
      output += ( num % 2 ).toString();
      num = Math.floor( num / 2 );
    }
    output += "1";
    return output.split("").reverse().join().replace(/,/g , "");
  }
})();